#!/bin/bash
#PBS -o /home/projects/cu_10027/people/anviol/sv/yilong/BRASS/perl/share/Rscripts/testingScript/output.out
#PBS -e /home/projects/cu_10027/people/anviol/sv/yilong/BRASS/perl/share/Rscripts/testingScript/error.err
#PBS -l mem=100gb
#PBS -l walltime=30:00:00
#PBS -N get_rg_cns_test


module load bedtools/2.27.1

PWD=/home/projects/cu_10027/people/anviol/sv/yilong/BRASS/perl/share/Rscripts/
#bedpe=/home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/03b5268e-881e-49e7-824f-170c3fc8b11b.pcawg_consensus_1.6.161116.somatic.sv.bedpe/03b5268e-881e-49e7-824f-170c3fc8b11b.pcawg_consensus_1.6.161116.somatic.sv.bedpe
bedpe=/home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/03b5268e-881e-49e7-824f-170c3fc8b11b.pcawg_consensus_1.6.161116.somatic.sv.bedpe_my_rg_cns/03b5268e-881e-49e7-824f-170c3fc8b11b.pcawg_consensus_1.6.161116.somatic.sv.bedpe.abs_bkpts
cn=/home/projects/cu_10027/people/anviol/master/test/cn_test.txt
segments=/home/projects/cu_10027/people/anviol/master/test/segmentation_test.txt
bam=/home/projects/cu_10027/projects/pcawg/data/data_processed/bam/03b5268e-881e-49e7-824f-170c3fc8b11b/tumor_ICGC_MB6_merged.mdup.bam
acf=0.973
cent=/home/projects/cu_10027/people/anviol/sv/yilong/data/cent_file.tsv
workDir=${PWD}/testingScript/

Rscript ${PWD}/get_rg_cns.R $bedpe $cn $segments $bam $acf $cent N N $workDir
