#!/bin/bash
#PBS -o /home/projects/cu_10027/people/anviol/sv/yilong/BRASS/perl/share/Rscripts/testingScript/output.out
#PBS -e /home/projects/cu_10027/people/anviol/sv/yilong/BRASS/perl/share/Rscripts/testingScript/error.err
#PBS -l mem=100gb
#PBS -l walltime=70:00:00
#PBS -N get_rg_cns_test


module load bedtools/2.27.1

PWD=/home/projects/cu_10027/people/anviol/sv/yilong/BRASS/perl/share/Rscripts/
bedpe=/home/projects/cu_10027/people/anviol/sv/yilong/data/Re__Insertion_and_microhomology_in_PCAWG_SV_calls/fc8130e0-0fde-b756-e040-11ac0c48328b.merged.abs_bkpts.bedpe
cn=/home/projects/cu_10027/people/anviol/sv/yilong/data/Re__Insertion_and_microhomology_in_PCAWG_SV_calls/fc8130e0-0fde-b756-e040-11ac0c48328b.ngscn.abs_cn.bg.head
segments=/home/projects/cu_10027/people/anviol/sv/yilong/data/Re__Insertion_and_microhomology_in_PCAWG_SV_calls/fc8130e0-0fde-b756-e040-11ac0c48328b.ngscn.segments.abs_cn.bg
bam=/home/projects/cu_10027/projects/pcawg/data/data_processed/bam/03b5268e-881e-49e7-824f-170c3fc8b11b/tumor_ICGC_MB6_merged.mdup.bam
acf=0.973
cent=/home/projects/cu_10027/people/anviol/sv/yilong/data/cent_file.tsv
workDir=${PWD}/testingScript/



Rscript ${PWD}/get_rg_cns.R $bedpe $cn $segments $bam $acf $cent N N $workDir
