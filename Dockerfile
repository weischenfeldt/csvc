FROM weischenfeldt/csvc:latest

# File Author / Maintainer
LABEL maintainer="francesco.favero@bric.ku.dk"

# Set versions
ENV PERL_VERSION 5.18.4
ENV HTSLIB_VERSION 1.9
ENV SAMTOOLS_VERSION 1.9
ENV BEDTOOLS_VERSION 2.22.1
ENV BIGWIG_VERSION 1.0.2
ENV KENT_VERSION 334


## Setup packages
USER root


ADD run_pipeline.py /usr/local/bin/run_csvc.py
ADD run_pipeline_fast.py /usr/local/bin/run_csvc_fast.py
ADD run_classification.py /usr/local/bin/run_classification.py

## Install pype modules
ADD pype_modules/repos.yaml /root/repos.yaml
RUN pype repos -r /root/repos.yaml install -f csvc \
    && chmod +x /usr/local/bin/run_csvc.py \
    && chmod +x /usr/local/bin/run_csvc_fast.py \
    && chmod +x /usr/local/bin/run_classification.py \
    && rm /root/repos.yaml

ADD r_scripts /opt/Rscripts

ADD sv_covplot /root/sv_covplot
RUN chmod +x /root/sv_covplot/bin/* \
    && mv /root/sv_covplot/bin/* /usr/local/bin/ \
    && rm -rf /root/sv_covplot


RUN yum clean all -q \
	&& rm -fr /var/cache/yum/* /tmp/* /var/tmp/* /root/.pki

USER docker

WORKDIR /home/docker

CMD ["/bin/bash"]
