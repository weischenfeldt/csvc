import os
import shlex
import subprocess
import gzip


def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    input = argv['--input']
    output = '%s.output' % input
    return({'out_key': output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Extract complex SV '
                                       'signatures'),
                                 add_help=False)


def extract_footprint_args(parser, subparsers, argv):
    parser.add_argument('--sample',  dest='sample',
                        help='Sample name', required=True)
    parser.add_argument('--purity',  dest='purity',
                        help='Sample purity estimate', required=True)
    parser.add_argument('--ploidy',  dest='ploidy',
                        help='Sample ploidy estimate', required=True)
    parser.add_argument('--cluster',  dest='cluster',
                        help='SV cluster', required=True)
    parser.add_argument('--classification',  dest='classification',
                        help='SV classification output', required=True)
    parser.add_argument('--breakpoints',  dest='bkpt',
                        help='SV -recalibrated- breakpoints', required=True)
    parser.add_argument('--rg',  dest='rg_cns',
                        help='RG copy number', required=True)
    return parser.parse_args(argv)


def extract_footprint(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = extract_footprint_args(add_parser(
        subparsers, module_name), subparsers, argv)

    extract_footprint_cmd = [
        'extract_footprint_analysis_stats.pl',
         '-acf', args.purity, '-ploidy', args.ploidy,
         '-sample_name', args.sample, '-sv_clusters_file', args.cluster,
         '-sv_classification', args.classification, args.bkpt, args.rg_cns]

    extract_footprint_cmd = shlex.split(
        ' '.join(map(str, extract_footprint_cmd)))
    log.log.info('Prepare the pcawg extract_footprint sv command line:')
    log.log.info(' '.join(map(str, extract_footprint_cmd)))
    extract_footprint_proc = subprocess.Popen(extract_footprint_cmd)
    out = extract_footprint_proc.communicate()[0]
    code = extract_footprint_proc.returncode
    info = 'Extract footprint exit code: %s' % code
    if code == 0:
        log.log.info(info)
    else:
        log.log.error(info)

    log.log.info('Terminate extract_footprint')
