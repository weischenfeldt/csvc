import os
import shlex
import subprocess
import gzip


def requirements():
    return({'ncpu': 2, 'time': '12:00:00'})


def results(argv):
    try:
        input = argv['--input']
    except KeyError:
        input = argv['-i']
    # input = re.sub('.gz$', '', input)
    # input = re.sub('.bedpe$', '', input)
    cluster_sv = '%s.sv_clusters_and_footprints' % input
    dist_pvals = '%s.sv_distance_pvals' % input

    return({'clusters': cluster_sv, 'dist_pvals': dist_pvals})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help=('Group rearrangements into rearrangement '
                           'clusters and footprints'), add_help=False)


def cluster_sv_args(parser, subparsers, argv):
    parser.add_argument('--input',  '-i', dest='input',
                        help='SV rearrangements file in bedpe format',
                        required=True)
    return parser.parse_args(argv)


def cluster_sv(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = cluster_sv_args(add_parser(
        subparsers, module_name), subparsers, argv)

    chr_sizes_file = profile.files['chr_sizes_file']
    cent_tel_file = profile.files['cent_tel_file']

    clusterSV = profile.programs['clusterSV']['path']
    Rscript = profile.programs['Rscript']['path']
    cluster_sv_cmd = [
        Rscript, clusterSV, args.input,
        chr_sizes_file, cent_tel_file, 2]
    cluster_sv_cmd = shlex.split(' '.join(map(str, cluster_sv_cmd)))
    log.log.info('Prepare cluster_sv command line:')
    log.log.info(' '.join(map(str, cluster_sv_cmd)))
    cluster_sv_proc = subprocess.Popen(cluster_sv_cmd)
    out = cluster_sv_proc.communicate()[0]
    code = cluster_sv_proc.returncode
    info = 'cluster SV exit code: %s' % code
    if code == 0:
        log.log.info(info)
    else:
        log.log.error(info)

    log.log.info('Terminate cluster_sv')
