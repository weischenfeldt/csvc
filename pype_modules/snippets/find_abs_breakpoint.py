import os
import shlex
import subprocess
import gzip


def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']

    return({'bedpe': output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Fine tune SV breakpoint'),
                                 add_help=False)


def find_abs_breakpoint_args(parser, subparsers, argv):
    parser.add_argument('--bam',  '-b', dest='bam',
                        help='Original Bam file used to call SVs',
                        required=True)
    parser.add_argument('--bedpe',  '-r', dest='bedpe',
                        help='SV calls in bedpe format',
                        required=True)
    parser.add_argument('--out',  '-o', dest='out',
                        help='Output file in bedpe format',
                        required=True)
    return parser.parse_args(argv)


def find_abs_breakpoint(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = find_abs_breakpoint_args(add_parser(
        subparsers, module_name), subparsers, argv)

    genome_fa = profile.files['genome_fa']
    abs_bkpts = profile.programs['abs_bkpts']['path']
    find_abs_breakpoint_cmd = [
        abs_bkpts, '--out', args.out, '--fasta',
        genome_fa, args.bam, args.bedpe]
    find_abs_breakpoint_cmd = shlex.split(
        ' '.join(map(str, find_abs_breakpoint_cmd)))
    log.log.info('Prepare the find_abs_breakpoint command line:')
    log.log.info(' '.join(map(str, find_abs_breakpoint_cmd)))
    find_abs_breakpoint_proc = subprocess.Popen(find_abs_breakpoint_cmd)
    out = find_abs_breakpoint_proc.communicate()[0]
    code = find_abs_breakpoint_proc.returncode
    info = 'Absolute breakpoint tuning exit code: %s' % code
    if code == 0:
        log.log.info(info)
    else:
        log.log.error(info)

    log.log.info('Terminate find_abs_breakpoint')
