import os
import shlex
import subprocess
import gzip
from pype.misc import xopen

def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    try:
        out_dir = argv['--out']
    except KeyError:
        out_dir = argv['-o']
    try:
        cn_file = argv['--cn']
    except KeyError:
        cn_file = argv['-c']
    cn_base = os.path.basename(cn_file)
    return({'rgs': os.path.join(out_dir, '%.rg_cns' % cn_base)})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Description of '
                                       'the tool'),
                                 add_help=False)


def generate_rgs_args(parser, subparsers, argv):
    parser.add_argument('--bedpe',  '-b', dest='bedpe',
                        help='Breakpoint in bedpe format',
                        required=True)
    parser.add_argument('--cn',  '-c', dest='cn',
                        help='Binned copy number file',
                        required=True)
    parser.add_argument('--segments',  '-s', dest='segments',
                        help='Segmentation file',
                        required=True)
    parser.add_argument('--tumor_bam',  '-t', dest='bam',
                        help='Tumor BAM file',
                        required=True)
    parser.add_argument('--solutions',  '-a', dest='sequenza_sol',
                        help='PPCG released cohort cellularity/ploidy results',
                        required=True)
    parser.add_argument('--sample_id',  '-i', dest='sample_id',
                        help='Sample ID from the cellularity/ploidy solution',
                        required=True)
    parser.add_argument('--gender_present',  dest='gender_present',
                        help='Gender present',
                        default='N')
    parser.add_argument('--gender_chromosome',  dest='gender_chromosome',
                        help='Gender Chromsome',
                        default='N')
    parser.add_argument('--out',  '-o', dest='output',
                        help='Output Folder',
                        required=True)
    return parser.parse_args(argv)


def generate_rgs(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = generate_rgs_args(add_parser(
        subparsers, module_name), subparsers, argv)

    cent_tel_file = profile.files['cent_tel_file2']

    generateRGS = profile.programs['generateRGS']['path']
    Rscript = profile.programs['Rscript']['path']

    sequenza_sol = get_sequenza_solutions(args.sequenza_sol, args.sample_id)
    acf = sequenza_sol['cellularity']
    generate_rgs_cmd = [
        Rscript, generateRGS, args.bedpe, args.cn, args.segments,
        args.bam, acf, cent_tel_file, args.gender_chromosome,
        args.gender_present, args.output]
    generate_rgs_cmd = shlex.split(' '.join(map(str, generate_rgs_cmd)))
    log.log.info('Prepare generate_rgs command line:')
    log.log.info(' '.join(map(str, generate_rgs_cmd)))
    generate_rgs_proc = subprocess.Popen(generate_rgs_cmd)
    out = generate_rgs_proc.communicate()[0]
    code = generate_rgs_proc.returncode
    info = 'generate RGS exit code: %s' % code
    if code == 0:
        log.log.info(info)
    else:
        log.log.error(info)

    log.log.info('Terminate generate_rgs')


def get_sequenza_solutions(sol, sample_id):
    cell = None
    ploidy = None
    with xopen(sol, 'rt') as sqz_sol:
        header = next(sqz_sol).strip().split(',')
        cell_idx = header.index('Cellularity')
        ploidy_idx = header.index('ploidy')
        ppcg_id_idx = header.index('PPCG_Sample_ID')
        for line in sqz_sol:
            line = line.strip().split(',')
            if line[ppcg_id_idx] == sample_id:
                cell = float(line[cell_idx])
                ploidy = float(line[ploidy_idx])
    if cell is None or ploidy is None:
        raise Exception('The sample %s was not found in the '
                        'solution file %s' % (sample_id, sol))
    else:
        return {'cellularity': cell, 'ploidy': ploidy}
