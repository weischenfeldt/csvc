import os
import shlex
import subprocess
from pype.misc import xopen

def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    try:
        output = argv['--output']
    except KeyError:
        output = argv['-o']
    try:
        sample_id = argv['--sample-id']
    except KeyError:
        sample_id = argv['-i']
    cn = os.path.join(output, '%s_cn.txt' % sample_id)
    segments = os.path.join(output, '%s_segmentation.txt' % sample_id)

    return({'cn': cn, 'segments': segments})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help=('Convert segmentation and coverage information '
                           'to the required formats'), add_help=False)


def conversion_tools_args(parser, subparsers, argv):
    parser.add_argument('--sample-id',  '-i', dest='id',
                        help='Sample ID',
                        required=True)
    parser.add_argument('--tumor',  '-t', dest='tumor',
                        help='Tumor scaled coverage file',
                        required=True)
    parser.add_argument('--control',  '-c', dest='control',
                        help='Control scales coverage file',
                        required=True)
    parser.add_argument('--segs',  '-s', dest='segs',
                        help='Segmentation file from Sequenza',
                        required=True)
    parser.add_argument('--solutions',  '-a', dest='sequenza_sol',
                        help='Alternative solutions file from sequenza',
                        required=True)
    parser.add_argument('--output',  '-o', dest='output',
                        help='Output folder',
                        required=True)
    return parser.parse_args(argv)


def conversion_tools(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = conversion_tools_args(add_parser(
        subparsers, module_name), subparsers, argv)
    sequenza_sol = get_sequenza_solutions(args.sequenza_sol, args.id)
    acf = sequenza_sol['cellularity']
    ploidy = sequenza_sol['ploidy']
    convertion_tools = profile.programs['convertion_tools']['path']
    Rscript = profile.programs['Rscript']['path']
    conversion_tools_cmd = [
        Rscript, convertion_tools, '-i', args.id, '-c', args.control,
        '-t', args.tumor, '-s', args.segs, '-p', ploidy, '-a',
        acf, '-o', args.output]
    conversion_tools_cmd = shlex.split(
        ' '.join(map(str, conversion_tools_cmd)))
    log.log.info('Prepare conversion_tools command line:')
    log.log.info(' '.join(map(str, conversion_tools_cmd)))
    conversion_tools_proc = subprocess.Popen(conversion_tools_cmd)
    out = conversion_tools_proc.communicate()[0]
    code = conversion_tools_proc.returncode
    info = 'Conversion Tools exit code: %s' % code
    if code == 0:
        log.log.info(info)
    else:
        log.log.error(info)

    log.log.info('Terminate conversion_tools')


def get_sequenza_solutions(sol, sample_id):
    cell = None
    ploidy = None
    with xopen(sol, 'rt') as sqz_sol:
        header = next(sqz_sol).strip().split(',')
        cell_idx = header.index('Cellularity')
        ploidy_idx = header.index('ploidy')
        ppcg_id_idx = header.index('PPCG_Sample_ID')
        for line in sqz_sol:
            line = line.strip().split(',')
            if line[ppcg_id_idx] == sample_id:
                cell = float(line[cell_idx])
                ploidy = float(line[ploidy_idx])
    if cell is None or ploidy is None:
        raise Exception('The sample %s was not found in the '
                        'solution file %s' % (sample_id, sol))
    else:
        return {'cellularity': cell, 'ploidy': ploidy}
