import re
import shlex
import subprocess
from pype.misc import human_format


def requirements():
    return({'ncpu': 1, 'time': '72:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    try:
        window = argv['--window']
    except KeyError:
        window = argv['-w']
    except KeyError:
        window = 1000
    w_size = human_format(int(window))
    output = re.sub('.gz$', '', output)
    output = re.sub('.cov$', '', output)
    if int(window) == 10000 or int(window) == 1000 or int(window) == 500:
        return({'cov': '%s_%s.cov.gz' % (output, w_size),
                'log': '%s_%s.cov.log' % (output, w_size),
                'gcnorm': '%s_%s.gcnorm.cov.gz' % (output, w_size)})
    else:
        return({'cov': '%s_%s.cov.gz' % (output, w_size),
                'log': '%s_%s.cov.log' % (output, w_size)})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Compute coverage with '
                                       'the cov tool from Delly'),
                                 add_help=False)


def cov_tool_args(parser, subparsers, argv):
    parser.add_argument('-b', '--bam', dest='bams', nargs='*',
                        help='One or more BAM file', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output name for the cov file',
                        required=True)
    parser.add_argument('-w', '--window', dest='window',
                        help='Wiondow size in nt', type=int,
                        default=500)
    return parser.parse_args(argv)


def cov_tool(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = cov_tool_args(add_parser(subparsers, module_name),
                         subparsers, argv)

    Rscript = profile.programs['Rscript']['path']
    cov_tool = profile.programs['cov_tool']['path']
    gc_norm_cov = profile.programs['gc_norm_cov']['path']

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)
    if args.window == 500:
        gc_bed = profile.files['gc_500_bed']
    elif args.window == 1000:
        gc_bed = profile.files['gc_1000_bed']
    elif args.window == 10000:
        gc_bed = profile.files['gc_10000_bed']

    log.log.info('Use GC file %s' % gc_bed)

    output = re.sub('.gz$', '', args.out)
    output = re.sub('.cov$', '', output)
    w_size = human_format(args.window)
    output = '%s_%s.cov.gz' % (output, w_size)

    log.log.info('Prepare cov command line')
    cov_cmd = [cov_tool, '-s', args.window, '-o', args.window,
               '-f', output]
    cov_cmd += args.bams

    log_out = '%s.log' % re.sub('.gz$', '', output)

    cov_cmd = shlex.split(' '.join(map(str, cov_cmd)))
    log.log.info(' '.join(map(str, cov_cmd)))

    log.log.info('Open file %s to store log information' % log_out)
    with open(log_out, 'wt') as log_cov:
        log.log.info('Execute cov with python subprocess.Popen')
        cov_proc = subprocess.Popen(cov_cmd, stdout=log_cov)
        out0 = cov_proc.communicate()[0]
    if args.window == 10000 or args.window == 1000 or args.window == 500:
        log.log.info('Prepare gc_norm command line')
        gc_out = '%s.gcnorm.cov.gz' % re.sub('.cov.gz', '', output)
        gc_cmd = [Rscript, gc_norm_cov, output, gc_bed, gc_out]
        gc_cmd = shlex.split(' '.join(map(str, gc_cmd)))
        log.log.info(' '.join(map(str, gc_cmd)))
        log.log.info('Execute gc_norm_cov with python subprocess.Popen')
        gc_proc = subprocess.Popen(gc_cmd)
        out1 = gc_proc.communicate()[0]
    log.log.info('Terminate cov_tool')
