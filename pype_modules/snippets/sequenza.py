import os
import shlex
import subprocess

SET_CPU = 4


def requirements():
    return({'ncpu': SET_CPU, 'time': '800:00:00', 'mem': '5gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    except KeyError:
        output = ''
    output = os.path.abspath(output)
    try:
        sample = argv['--sample']
    except KeyError:
        sample = argv['-s']
    sample_path = os.path.join(output, sample)
    return({'segments': '%s_segments.txt' % sample_path,
            'mutations': '%s_mutations.txt' % sample_path,
            'solutions': '%s_alternative_solutions.txt' % sample_path})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help=('Allele specific copy number '
                           'estimation from tumor genomes'),
        add_help=False)


def sequenza_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='Input seqz file', required=True)
    parser.add_argument('-s', '--sample', dest='sample',
                        help='Sample name', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output path')
    parser.add_argument('-b', '--breaks', dest='breaks', type=str,
                        help='BED file defining the breakpoints',
                        required=False)
    parser.add_argument('-x', '--x-heterozygous', dest='female',
                        help=('Flag to set when the X chromomeme '
                              'is heterozygous. eg: set it for '
                              'female genomes'), action='store_true')
    parser.add_argument('--ignore_normal', dest='ignore_normal',
                        help=('Use the GC-normalized tumor depth instead '
                              'of computing the depth-ratio '
                              'vs the normal sample'), action='store_true')
    parser.add_argument('--ratio_priority', dest='ratio_priority',
                        help=('Ignore the B-allele frequency when '
                              'fit the segments to a model'),
                        action='store_true')
    parser.add_argument('--cellularity', dest='cellularity', type=float,
                        help=('Run sequenza with a pre-defined cellularity '
                              'value. A number between 0 and 1'),
                        required=False)
    parser.add_argument('--ploidy', dest='ploidy', type=float,
                        help=('Run sequenza with a pre-defined ploidy '
                              'value. A number between 0.9 and 10, 2 for a '
                              'diploid genome'),
                        required=False)
    parser.add_argument('--cellularity-range', dest='cellularity_range',
                        type=cellularity_range, metavar='0-1',
                        help=('Limit the cellularity search to a '
                              'given range. Two numbers each between 0 and 1'
                              'separated by "-". Default 0-1'),
                        default='0-1')
    parser.add_argument('--ploidy-range', dest='ploidy_range',
                        type=ploidy_range, metavar='0.9-10',
                        help=('Limit the ploidy search to a '
                              'given range. Two numbers each between '
                              '0.9 and 10 separated by "-". Default 1-7'),
                        default='1-7')
    parser.add_argument('-n', '--ncpu', dest='ncpu', type=int,
                        help='Number of CPU, defalt %s' % SET_CPU,
                        default=SET_CPU)
    return parser.parse_args(argv)


def sequenza(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = sequenza_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Prepare R code')
    R = profile.programs['R']['path']

    if args.ignore_normal:
        ignore_normal = 'TRUE'
    else:
        ignore_normal = 'FALSE'
    log.log.info(('Read --ignore_normal argument, set sequenza '
                  'argument "ignore_normal=%s"') % ignore_normal)

    if args.female:
        female = 'TRUE'
    else:
        female = 'FALSE'
    log.log.info(('Read -x argument, set sequenza '
                  'argument "female=%s"') % female)

    if args.ratio_priority:
        ratio_priority = 'TRUE'
    else:
        ratio_priority = 'FALSE'
    log.log.info(('Read --ratio_priority argument, set sequenza '
                  'argument "ratio_priority=%s"') % ratio_priority)

    if args.cellularity:
        if 1 >= args.cellularity >= 0:
            cellularity = args.cellularity
        else:
            cell_message = (('Cellularity value out of range. '
                             '1 >= %s >= 0') % args.cellularity)
            log.log.error(cell_message)
            raise Exception(cell_message)
    else:
        cellularity = 'NULL'
    log.log.info(('Read --cellularity argument, set sequenza '
                  'argument "cellularity=%s"') % cellularity)

    if args.breaks:
        # validate the bed format
        bed_header = False
        is_bed = True
        with open(args.breaks, 'rt') as genome_breaks:
            bed_head = next(genome_breaks)
            try:
                bed_line = split_bed_line(bed_head)
                if bed_line[2] - bed_line[1] <= 0:
                    is_bed = False
            except ValueError:
                bed_header = True
            for line in genome_breaks:
                try:
                    bed_line = split_bed_line(line)
                    if bed_line[2] - bed_line[1] <= 0:
                        is_bed = False
                except ValueError:
                    is_bed = False
        if is_bed is True:
            breaks = args.breaks
            if bed_header is True:
                breaks_header = 'TRUE'
            else:
                breaks_header = 'FALSE'
        else:
            log.log.error(('The file %s is not a valid '
                           'BED file') % args.breaks)
            raise Exception(('The file %s is not a valid '
                             'BED file') % args.breaks)
            breaks = 'NULL'
            breaks_header = 'NULL'
    else:
        breaks = 'NULL'
        breaks_header = 'NULL'
    log.log.info(('Read --breaks argument, set sequenza '
                  'argument "breaks=%s"') % breaks)

    if args.ploidy:
        if 10 >= args.ploidy >= 0.9:
            ploidy = args.ploidy
        else:
            ploid_message = (('Ploidy value out of range. '
                              '10 >= %s >= 0.9') % args.ploidy)
            log.log.error(ploid_message)
            raise Exception(ploid_message)
    else:
        ploidy = 'NULL'
    log.log.info(('Read --ploidy argument, set sequenza '
                  'argument "ploidy=%s"') % ploidy)

    r_code = '''
    library(sequenza)
    if ("%(breaks)s" == "NULL") {
        breaks <- NULL
    } else {
        breaks <- read.table("%(breaks)s", header = %(breaks_header)s,
            sep = "\t", stringsAsFactors = FALSE)
        breaks <- breaks[, 1:3]
        colnames(breaks) <- c("chrom", "start.pos", "end.pos")
    }
    extract <- sequenza.extract("%(input_seqz)s", breaks = breaks,
                                ignore.normal = %(ignore_normal)s)
    CP <- sequenza.fit(extract, female = %(female)s,
                       cellularity = seq(%(cell_min)s, %(cell_max)s, 0.01),
                       ploidy = seq(%(ploidy_min)s, %(ploidy_max)s, 0.1),
                       ratio.priority = %(ratio_priority)s,
                       mc.cores = %(mc_cores)i)
    sequenza.results(sequenza.extract = extract, cp.table = CP,
                     sample.id = "%(sample_name)s",
                     female = %(female)s,
                     out.dir = "%(output_path)s",
                     cellularity = %(cellularity)s,
                     ploidy = %(ploidy)s)
    ''' % {'input_seqz': args.input,
           'sample_name': args.sample,
           'female': female,
           'breaks': breaks,
           'breaks_header': breaks_header,
           'ignore_normal': ignore_normal,
           'mc_cores': args.ncpu,
           'cell_min': args.cellularity_range[0],
           'cell_max': args.cellularity_range[1],
           'ratio_priority': ratio_priority,
           'ploidy_min': args.ploidy_range[0],
           'ploidy_max': args.ploidy_range[1],
           'output_path': args.out,
           'cellularity': cellularity,
           'ploidy': ploidy}

    log.log.info('Prepare R command line')
    R_cmd = [R, '--vanilla', '--slave']

    R_cmd = shlex.split(' '.join(map(str, R_cmd)))

    log.log.info(' '.join(map(str, R_cmd)))
    log.log.info('Execute R with python subprocess.Popen')
    sequenza_proc = subprocess.Popen(R_cmd, stdin=subprocess.PIPE)
    log.log.info('Pipe R code into R command line: %s'
                 % r_code)
    sequenza_proc.stdin.write(r_code)
    sequenza_proc.stdin.close()
    sequenza_proc.communicate()[0]

    log.log.info('Terminate sequenza')


def parse_range(string):
    try:
        values = map(float, string.strip().split('-'))
    except ValueError:
        raise ValueError(('Values in the range %s need to '
                          'be numeric') % string.strip().split('-'))
    if len(values) != 2:
        raise Exception(('Impossiblo to parse min and '
                         'max values from %s') % string)
    else:
        if values[0] != values[1]:
            return values
        else:
            raise Exception(('Min and max values in %s are '
                             'the same number!') % string)


def cellularity_range(string):
    values = sorted(parse_range(string))
    if 1 >= values[0] >= 0 and 1 >= values[1] >= 0:
        return values
    else:
        raise Exception(('Cellularity value out of range. 1 >= %s >= 0 and '
                         '1 >= %s >= 0') % (values[0], values[1]))


def ploidy_range(string):
    values = sorted(parse_range(string))
    if 10 >= values[0] >= 0.9 and 10 >= values[1] >= 0.9:
        return values
    else:
        raise Exception(('Ploidy value out of range. 10 >= %s >= 0.9 and '
                         '10 >= %s >= 0.9') % (values[0], values[1]))


def split_bed_line(bedline):
    try:
        chromosome, start, end, data = bedline.strip().split('\t', 3)
    except ValueError:
        chromosome, start, end = bedline.strip().split('\t')
    return(chromosome, int(start), int(end))
