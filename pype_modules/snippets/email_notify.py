import os
import smtplib
from email import Encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart


def requirements():
    return({'ncpu': 1, 'time': '00:30:00'})


def results(argv):
    return({})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Send email to notify '
                                       'finished process'),
                                 add_help=False)


def email_notify_args(parser, subparsers, argv):
    parser.add_argument('-f', '--from', dest='origin', type=str,
                        help='Sender email address', required=True)
    parser.add_argument('-t', '--to', dest='to', nargs='*',
                        help='Email recipients to send the message',
                        required=True)
    parser.add_argument('-s', '--subject', dest='subject',
                        help='Message subject', type=str, required=True)
    parser.add_argument('-b', '--body', dest='body',
                        help='Message body', type=str, required=True)
    parser.add_argument('--server', dest='smtp', type=str,
                        help='SMTP server address, default localhost',
                        default='localhost')
    parser.add_argument('-a', '--attachment', dest='attachments', nargs='*',
                        help='Attachment files', default=[])
    return parser.parse_args(argv)


def email_notify(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = email_notify_args(add_parser(subparsers, module_name),
                             subparsers, argv)
    pype_address = args.origin
    smtp_server = args.smtp

    log.log.info('Preparing email')

    msg = MIMEMultipart()
    msg.preamble = 'This is a multi-part message in MIME format.\n'
    msg.epilogue = ''
    body = MIMEMultipart('alternative')
    body.attach(MIMEText(args.body))
    msg.attach(body)

    for filename in args.attachments:
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(filename, 'rt').read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' %
                        os.path.basename(filename))
        msg.attach(part)

    msg['Subject'] = args.subject
    msg['To'] = ', '.join(args.to)

    log.log.info('Sending email as %s with smtp %s' % (
        pype_address, smtp_server))
    msg['From'] = pype_address
    s = smtplib.SMTP(smtp_server)
    s.sendmail(pype_address, args.to, msg.as_string())
    s.quit()
    log.log.info('Terminate email_notify')
