#!/usr/bin/env python2.7

import os
import sys
import gzip
import argparse
import logging


def xopen(filename, mode='r'):
    '''
    Replacement for the "open" function that can also open
    files that have been compressed with gzip. If the filename ends with .gz,
    the file is opened with gzip.open(). If it doesn't, the regular open()
    is used. If the filename is '-', standard output (mode 'w') or input
    (mode 'r') is returned.
    '''
    assert isinstance(filename, str)
    if filename == '-':
        return sys.stdin if 'r' in mode else sys.stdout
    if filename.endswith('.gz'):
        return gzip.open(filename, mode)
    else:
        return open(filename, mode)


def median(lst):
    sortedLst = sorted(lst)
    lstLen = len(lst)
    index = (lstLen - 1) // 2
    if (lstLen % 2):
        return sortedLst[index]
    else:
        return (sortedLst[index] + sortedLst[index + 1]) / 2.0


def scale_coverage(refence, cov):
    scale_list = list()
    with xopen(refence, 'rt') as ref_file, xopen(cov, 'rt') as cov_file:
        for line in ref_file:
            chrom_ref, start_ref, end_ref, reads_ref = line.strip().split('\t')
            chrom_cov, start_cov, end_cov, reads_cov = next(
                cov_file).strip().split('\t')
            if chrom_cov == chrom_ref and start_ref == start_cov and \
                    end_ref == end_cov:
                reads_cov = float(reads_cov)
                reads_ref = float(reads_ref)
                if reads_cov > 0 and reads_ref > 0:
                    scale_list.append(reads_cov / reads_ref)
            else:
                raise Exception(('Coordinate mismatch betwee file'
                                 '%s and fila %s') % (refence, cov))
    return(1 / median(scale_list))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--reference',  dest='ref_cov',
                        help='Coverage file to use as reference',
                        type=str, required=True)
    parser.add_argument('-c', '--coverages',  dest='covs', nargs='*',
                        help='Coverage files to normalize', required=True)
    parser.add_argument('-o', '--output',  dest='out', nargs='*',
                        help=('Output files, it must contains as many '
                              'items as the --coverages argument'),
                        default=['-'])
    args = parser.parse_args()

    logging.basicConfig(format='%(levelname)s: %(message)s',
                        level=logging.DEBUG)
    if len(args.covs) == len(args.out):
        outputs = iter(args.out)
        for cov in args.covs:
            scale_factor = scale_coverage(args.ref_cov, cov)
            cov_path, cov_name = os.path.split(cov)
            logging.info('%s scale factor: %s' % (args.ref_cov, 1))
            logging.info('%s scale factor: %s' % (cov, scale_factor))
            res_file = next(outputs)
            logging.info('Writing scaled coverage file to: %s' % res_file)
            with xopen(cov, 'rt') as cov_file, xopen(res_file, 'wt') as out:
                for line in cov_file:
                    line = line.strip().split('\t')
                    line[3] = float(line[3]) * scale_factor
                    out.write('%s\n' % '\t'.join(map(str, line)))
    else:
        msg = ('Arguments "-c %s" and "-o %s" '
               'are not of the same lenght') % (
            ' '.join(args.covs), ' '.join(args.out))
        logging.error(msg)

if __name__ == "__main__":
    main()

