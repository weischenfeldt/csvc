#!/usr/bin/env python2.7

import argparse
import os
import sys
import re
import gzip
import shlex
import subprocess
import tarfile
import logging
from pype.modules.profiles import get_profiles
from pype.logger import ExtLog
from contextlib import closing


def xopen(filename, mode='r'):
    '''
    Replacement for the "open" function that can also open
    files that have been compressed with gzip. If the filename ends with .gz,
    the file is opened with gzip.open(). If it doesn't, the regular open()
    is used. If the filename is '-', standard output (mode 'w') or input
    (mode 'r') is returned.
    '''
    assert isinstance(filename, str)
    if filename == '-':
        return sys.stdin if 'r' in mode else sys.stdout
    if filename.endswith('.gz'):
        return gzip.open(filename, mode)
    else:
        return open(filename, mode)


def create_symlinks(ref_dict, profile, log):
    '''
    Practical function to create symbolic links
    to the input files corresponding in the
    profile.files list
    '''
    for key in ref_dict:
        log.log.info('Handle reference file: %s' % key)
        if ref_dict[key]:
            log.log.info('Symlink %s to %s' % (ref_dict[key],
                                               profile.files[key]))
            os.symlink(ref_dict[key], profile.files[key])


def prepare_tmp_dirs(tempdir, log, subdirs=['databases', 'data', 'runs']):
    if not os.path.isdir(tempdir):
        os.makedirs(tempdir)
    for subdir in subdirs:
        subdir_tmp = os.path.join('/tmp', subdir)
        subdir = os.path.join(tempdir, subdir)
        if not os.path.isdir(subdir):
            log.log.info('Prepare temporary folder %s' % subdir)
            os.mkdir(subdir)
            if subdir_tmp != subdir:
                if not os.path.exists(subdir_tmp):
                    log.log.info('Symlink temporary folder %s to %s' %
                                 (subdir, subdir_tmp))
                    os.symlink(subdir, subdir_tmp)
                else:
                    log.log.error('The temporary folder %s already exists' %
                                  subdir_tmp)


def check_returncode(process, title, command, log):
    if process.returncode == 0:
        log.log.info('%s with command %s succeeded' % (title, command))
    else:
        log.log.error('%s with command %s failed' % (title, command))
        raise Exception('Error while executing %s' % command)


FILE_LIST_SUFFIX = (
    'cn.txt.rg_cns',
    'sv_clusters_and_footprints',
    'sv_abs_breakpoins.bedpe'
)


def get_required_files(members):
    for tarinfo in members:
        if any(map(tarinfo.name.endswith, FILE_LIST_SUFFIX)):
            yield tarinfo


def setup_rg_tar(tar_path, out_path, profile, log):

    with closing(tarfile.open(tar_path, 'r:gz')) as res:
        required_files = list(get_required_files(res))
        res.extractall(path=out_path, members=required_files)

        log.log.info('Extract tar file %s to %s' % (tar_path, out_path))
        for member in res:
            which_match = map(member.name.endswith, FILE_LIST_SUFFIX)
            if any(which_match):
                print(which_match)
                print(member.name)
                if which_match[0] is True:
                    rg_out = os.path.join(out_path, member.name)
                if which_match[1] is True:
                    cluster_out = os.path.join(out_path, member.name)
                if which_match[2] is True:
                    bedpe_out = os.path.join(out_path, member.name)
    
    return {'bedpe': bedpe_out, 'cluster': cluster_out,
            'rg_cns': rg_out}


def get_ppcg_solutions(sol, sample_id, log):
    cell = None
    ploidy = None
    log.log.info('Open solution file %s to retrieve cellularity and ploidy' % sol)
    with xopen(sol, 'rt') as ppcg_sol:
        header = next(ppcg_sol).strip().split(',')
        cell_idx = header.index('Cellularity')
        ploidy_idx = header.index('ploidy')
        ppcg_id_idx = header.index('PPCG_Sample_ID')
        for line in ppcg_sol:
            line = line.strip().split(',')
            if line[ppcg_id_idx] == sample_id:
                cell = float(line[cell_idx])
                ploidy = float(line[ploidy_idx])
    if cell is None or ploidy is None:
        log.log.error('The sample %s was not found in the '
                      'solution file %s' % (sample_id, sol))
        raise Exception('The sample %s was not found in the '
                        'solution file %s' % (sample_id, sol))
    else:
        log.log.info('Found sample %s cellularity: %s and '
                     'ploidy %s' % (sample_id, cell, ploidy))
        return {'cellularity': cell, 'ploidy': ploidy}


def main():
    parser = argparse.ArgumentParser(
        description=('Run cSVc pipeline in bio_pype'))
    parser.add_argument('--sample-id',  dest='sample',
                        help='Sample id, identifier of the run',
                        required=True)
    parser.add_argument('--rg-tar', dest='rg_tar',
                        help='RG tar file, from csvc pipeline',  required=True)
    parser.add_argument('--solutions', dest='sols',
                        help='Sequenza alternative solutions file',
                        required=True)
    parser.add_argument('--mem',  dest='mem',
                        help=('Amount of max GB of memory to use. '
                              'Default: autodetect'),
                        type=int, required=False)
    parser.add_argument('--ncpu',  dest='ncpu',
                        help='Number of cpu to use. Default: autodetect',
                        type=int, required=False)
    parser.add_argument('--no_archive',  dest='no_arch',
                        help='Set to avoid tar of output',
                        action='store_true')
    parser.add_argument('--tmp',  dest='tempdir',
                        help='Set the temporary folder',
                        default='/tmp')

    args = parser.parse_args()
    archive_res = not args.no_arch
    if args.mem:
        os.environ['PYPE_MEM'] = '%iG' % args.mem
    if args.ncpu:
        os.environ['PYPE_NCPU'] = '%i' % args.ncpu
    try:
        tempdir = os.environ['TEMPDIR']
    except KeyError:
        tempdir = args.tempdir

    log_dir = os.path.join(os.getcwd(), 'logs')
    log = ExtLog('run_csvc_classification', log_dir, level=logging.INFO)

    log.log.info('Prepare temporary diirectory structure')
    prepare_tmp_dirs(tempdir, log, ['databases', 'data', 'workdir'])

    output_dir = '/tmp/workdir'
    results_dir = os.getcwd()

    log.log.info('Output results in folder %s' % output_dir)
    use_profile = 'default'
    log.log.info('Use profile %s' % use_profile)
    profile = get_profiles({})[use_profile]

    rg_files = setup_rg_tar(
        args.rg_tar, '/tmp/data', profile, log)
    out_dirs = [
        os.path.join(output_dir, 'classification')]
    for out_dir in out_dirs:
        if not os.path.isdir(out_dir):
            os.makedirs(out_dir)
    solutions = get_ppcg_solutions(args.sols, args.sample, log)

    classification_file_name = '%s_complex_sv_classification.bedpe' % args.sample
    pype_cmd = ['pype', '--profile', use_profile, 'snippets',
                '--log', log_dir,
                'extract_footprint',
                '--sample', args.sample,
                '--purity', solutions['cellularity'],
                '--ploidy', solutions['ploidy'],
                '--breakpoints', rg_files['bedpe'],
                '--cluster', rg_files['cluster'],
                '--classification', os.path.join(
                    output_dir, 'classification', classification_file_name),
                '--rg', rg_files['rg_cns']
                ]

    pype_cmd = shlex.split(' '.join(map(str, pype_cmd)))
    log.log.info('Prepare pype command line:')
    log.log.info(' '.join(map(str, pype_cmd)))
    pype_proc = subprocess.Popen(pype_cmd)
    pype_proc.communicate()[0]

    if archive_res:
        rgs_dir = os.path.join(output_dir, 'classification')

        rgs_res_file = os.path.join(
            results_dir, '%s_classification.tar.gz' % args.sample)
        log.log.info(
            'Create archive for results files in %s' % rgs_res_file)
        rgs_tar = tarfile.open(rgs_res_file, 'w:gz')
        rgs_tar.add(
            rgs_dir, arcname='classification')
        rgs_tar.close()

        log.log.info('Archive log directory %s' % log_dir)
        tar = tarfile.open(
            os.path.join(results_dir, '%s_logs.tar.gz' % args.sample), 'w:gz')
        tar.add(log_dir, arcname='logs')
        tar.close()
    else:
        log.log.info('Skip archive of results')
    log.log.info('Done')

if __name__ == '__main__':
    main()
