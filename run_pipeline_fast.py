#!/usr/bin/env python2.7

import argparse
import os
import sys
import re
import gzip
import shlex
import subprocess
import tarfile
import logging
from pype.modules.profiles import get_profiles
from pype.logger import ExtLog


def xopen(filename, mode='r'):
    '''
    Replacement for the "open" function that can also open
    files that have been compressed with gzip. If the filename ends with .gz,
    the file is opened with gzip.open(). If it doesn't, the regular open()
    is used. If the filename is '-', standard output (mode 'w') or input
    (mode 'r') is returned.
    '''
    assert isinstance(filename, str)
    if filename == '-':
        return sys.stdin if 'r' in mode else sys.stdout
    if filename.endswith('.gz'):
        return gzip.open(filename, mode)
    else:
        return open(filename, mode)


def create_symlinks(ref_dict, profile, log):
    '''
    Practical function to create symbolic links
    to the input files corresponding in the
    profile.files list
    '''
    for key in ref_dict:
        log.log.info('Handle reference file: %s' % key)
        if ref_dict[key]:
            log.log.info('Symlink %s to %s' % (ref_dict[key],
                                               profile.files[key]))
            os.symlink(ref_dict[key], profile.files[key])


def prepare_tmp_dirs(tempdir, log, subdirs=['databases', 'data', 'runs']):
    if not os.path.isdir(tempdir):
        os.makedirs(tempdir)
    for subdir in subdirs:
        subdir_tmp = os.path.join('/tmp', subdir)
        subdir = os.path.join(tempdir, subdir)
        if not os.path.isdir(subdir):
            log.log.info('Prepare temporary folder %s' % subdir)
            os.mkdir(subdir)
            if subdir_tmp != subdir:
                if not os.path.exists(subdir_tmp):
                    log.log.info('Symlink temporary folder %s to %s' %
                                 (subdir, subdir_tmp))
                    os.symlink(subdir, subdir_tmp)
                else:
                    log.log.error('The temporary folder %s already exists' %
                                  subdir_tmp)


def check_returncode(process, title, command, log):
    if process.returncode == 0:
        log.log.info('%s with command %s succeeded' % (title, command))
    else:
        log.log.error('%s with command %s failed' % (title, command))
        raise Exception('Error while executing %s' % command)


def gcnorm_cov(cov_file, output, profile, log):
    Rscript = profile.programs['Rscript']['path']
    gc_norm_cov = profile.programs['gc_norm_cov']['path']
    gc_bed = profile.files['gc_1000_bed']
    log.log.info('Use GC file %s' % gc_bed)

    log.log.info('Prepare gc_norm command line')
    gc_cmd = [Rscript, gc_norm_cov, cov_file, gc_bed, output]
    gc_cmd = shlex.split(' '.join(map(str, gc_cmd)))
    log.log.info(' '.join(map(str, gc_cmd)))
    log.log.info('Execute gc_norm_cov with python subprocess.Popen')
    gc_proc = subprocess.Popen(gc_cmd)
    out1 = gc_proc.communicate()[0]
    log.log.info('Terminate GC norm of %s' % cov_file)


def setup_bams_bedpe(tumor, tumor_bai, bedpe, tumor_cov,
                     normal_cov, bam_path, profile, log):

    tumor_link = os.path.join(bam_path, 'tumor.bam')
    tumor_bai_link = os.path.join(bam_path, 'tumor.bam.bai')
    bedpe_link = os.path.join(bam_path, 'tumor.bedpe')
    tumor_cov_link = os.path.join(bam_path, 'tumor_gcnorm.cov.gz')
    normal_cov_link = os.path.join(bam_path, 'normal_gcnorm.cov.gz')

    ## In the fast pipeline we need to run gcnorm on the 
    ## cov files (instead of just symlink in the bam folder)

    log.log.info('GC normalize %s to %s' % (tumor_cov, tumor_cov_link))
    gcnorm_cov(tumor_cov, tumor_cov_link, profile, log)
    log.log.info('GC normalize %s to %s' % (normal_cov, normal_cov_link))
    gcnorm_cov(normal_cov, normal_cov_link, profile, log)

    log.log.info('Symlink %s to %s' % (tumor, tumor_link))
    os.symlink(tumor, tumor_link)
    
    ## Instead of symlink we need to remove the header line
    log.log.info('Remove comments/header from %s to %s' % (
        bedpe, bedpe_link))
    with xopen(
         bedpe, 'rt') as bedpe_in, xopen(
            bedpe_link, 'wt') as bedpe_link_in:
        header = next(bedpe_in)
        for line in bedpe_in:
            if not line.startswith('#'):
                bedpe_link_in.write(line)


    idx_tumor = ['samtools', 'index', tumor_link]
    idx_tumor = shlex.split(' '.join(map(str, idx_tumor)))
    if tumor_bai is None:
        log.log.info('Index %s' % ' '.join(map(str, idx_tumor)))
        idx_tumor_proc = subprocess.Popen(idx_tumor)
        idx_tumor_proc.communicate()[0]
        check_returncode(idx_tumor_proc, 'tumor bam index',
                         ' '.join(map(str, idx_tumor)), log)
    else:
        log.log.info('Symlink %s to %s' % (tumor_bai, tumor_bai_link))
        os.symlink(tumor_bai, tumor_bai_link)

    return {'tumor': tumor_link, 'tumor_cov': tumor_cov_link,
            'normal_cov': normal_cov_link, 'bedpe': bedpe_link}


def setup_ref_files(cent_tel, cent_tel2, wig, gc_1kbed,
                    bgzip_ref, profile_obj, log):

    idx_genome = ['samtools', 'faidx', profile_obj.files['genome_fa']]
    idx_genome2 = ['samtools', 'faidx', profile_obj.files['genome_fa_gz']]
    bgz_genome = ['bgzip', '-c', '-f', profile_obj.files['genome_fa']]
    gc_window_bed = profile_obj.programs['gc_window_bed']['path']

    # Symlink centromere telomere files.
    # It needs fixes after testing (eg test if we can
    # avoid to have 2 different formats)
    create_symlinks({'cent_tel_file': cent_tel}, profile_obj, log)
    create_symlinks({'cent_tel_file2': cent_tel2}, profile_obj, log)

    idx_genome = shlex.split(' '.join(map(str, idx_genome)))
    idx_genome2 = shlex.split(' '.join(map(str, idx_genome2)))
    bgz_genome = shlex.split(' '.join(map(str, bgz_genome)))

    def bgzip_fa(idx_genome2, bgz_genome, profile_obj, log):
        with open(profile_obj.files['genome_fa_gz'], 'wt') as genome_fa_gz:
            log.log.info('Bgzip %s to %s' % (
                profile_obj.files['genome_fa'],
                profile_obj.files['genome_fa_gz']))
            bgzip_genome_proc = subprocess.Popen(
                bgz_genome, stdout=genome_fa_gz)
            bgzip_genome_proc.communicate()[0]
            check_returncode(bgzip_genome_proc, 'genome.fa bgzip',
                             ' '.join(map(str, bgz_genome)), log)

    def uzip_fa(profile_obj, log):
        with open(profile_obj.files['genome_fa'], 'wt') as genome_fa:
            try:
                log.log.info('Unzip %s to %s' % (
                    profile_obj.files['genome_fa_gz'],
                    profile_obj.files['genome_fa']))
                with gzip.open(profile_obj.files['genome_fa_gz'], 'rt') \
                        as genome_fa_gz:
                    for line in genome_fa_gz:
                        genome_fa.write(line)
            except IOError:
                log.log.warning('IOError while unzip %s' %
                                profile_obj.files['genome_fa_gz'])

    if bgzip_ref is True:
        bgzip_fa(idx_genome2, bgz_genome, profile_obj, log)
    else:
        uzip_fa(profile_obj, log)

    log.log.info('Index %s' % ' '.join(map(str, idx_genome2)))
    idx_genome2_proc = subprocess.Popen(idx_genome2)
    idx_genome2_proc.communicate()[0]
    genome_gz_fai = '%s.fai' % profile_obj.files['genome_fa_gz']
    if os.path.isfile(genome_gz_fai):
        bgzip_ref = False
    else:
        log.log.warning(('Failed to index genome.gz, '
                         'will attempt to recover by '
                         're-compressing with bgzip'))
        bgzip_ref = True

    log.log.info('Index %s' % ' '.join(map(str, idx_genome)))
    idx_genome_proc = subprocess.Popen(idx_genome)
    idx_genome_proc.communicate()[0]
    check_returncode(idx_genome_proc, 'genome.fa index',
                     ' '.join(map(str, idx_genome)), log)
    if bgzip_ref is True:
        os.unlink(profile_obj.files['genome_fa_gz'])
        bgzip_fa(idx_genome2, bgz_genome, profile_obj, log)
        log.log.info('Index %s' % ' '.join(map(str, idx_genome2)))
        idx_genome2_proc = subprocess.Popen(idx_genome2)
        idx_genome2_proc.communicate()[0]
        check_returncode(idx_genome2_proc, 'genome.fa.gz index',
                         ' '.join(map(str, idx_genome2)), log)
    # Generate the genome lenght file from the genome.fa index
    genome_len_cmd = ['cut', '-f1,2', '%s.fai' %
                      profile_obj.files['genome_fa']]
    genome_len_cmd = shlex.split(' '.join(map(str, genome_len_cmd)))
    genome_len_file = profile_obj.files['chr_sizes_file']
    log.log.info('Open in write mode file %s' % genome_len_file)
    with open(genome_len_file, 'wt') as genome_len:
        log.log.info('Output command %s in file %s' %
                     (' '.join(map(str, genome_len_cmd)), genome_len_file))
        genome_len_proc = subprocess.Popen(genome_len_cmd, stdout=genome_len)
        genome_len_proc.communicate()[0]
        check_returncode(genome_len_proc, 'genome lenght',
                         ' '.join(map(str, genome_len_cmd)), log)
    gc_bed = [gc_window_bed, profile_obj.files['genome_fa']]
    gc_bed_1k = gc_bed + [1000, 1000]
    gc_bed_1k = shlex.split(' '.join(map(str, gc_bed_1k)))
    if gc_1kbed is None:
        log.log.info('GC bed 1000 %s' % ' '.join(map(str, gc_bed_1k)))
        with open(profile_obj.files['gc_1000_bed'], 'wt') as gc_bed_out:
            gc_bed_1k_proc = subprocess.Popen(gc_bed_1k, stdout=gc_bed_out)
            gc_bed_1k_proc.communicate()[0]
            check_returncode(gc_bed_1k_proc, 'GC bed 1000',
                             ' '.join(map(str, gc_bed_1k)), log)
    else:
        log.log.info('Symlink %s to %s' % (
            gc_1kbed, profile_obj.files['gc_1000_bed']))
        os.symlink(gc_1kbed, profile_obj.files['gc_1000_bed'])

def main():
    parser = argparse.ArgumentParser(
        description=('Run cSVc pipeline in bio_pype'))
    parser.add_argument('--sample-id',  dest='sample',
                        help='Sample id, identifier of the run',
                        required=True)
    parser.add_argument('--tumor-bam', dest='tumor_bam',
                        help='Tumor BAM file',  required=True)
    parser.add_argument('--tumor-bam-index',  dest='tumor_bai',
                        help='Tumor Bam index file',  required=False)
    parser.add_argument('--tumor-cov', dest='tumor_cov',
                        help='Tumor coverage file',  required=True)
    parser.add_argument('--normal-cov', dest='normal_cov',
                        help='Normal/control coverage file',  required=True)
    parser.add_argument('--breakpoins', dest='svs',
                        help='Structural variation breakpoint in bedpe format',
                        required=True)
    parser.add_argument('--solutions', dest='sols',
                        help='Sequenza alternative solutions file',
                        required=True)
    parser.add_argument('--segments', dest='segs',
                        help='Sequenza segmentation file',
                        required=True)
    parser.add_argument('--reference-gz', dest='ref_gz',
                        help=('Genome reference gz-compressed file '
                              '(or plain text)'),
                        required=True)
    parser.add_argument('--gc_1kbed', dest='gc1kb',
                        help='GC content 1000 nt bin size, in bed format')
    parser.add_argument('--centromere-telomeres', dest='cen_tel',
                        help='Centromere and telomeres coordinates',
                        required=True)
    parser.add_argument('--centromere-telomeres2', dest='cen_tel2',
                        help='Sorted entromere and telomeres coordinates',
                        required=True)
    parser.add_argument('--mem',  dest='mem',
                        help=('Amount of max GB of memory to use. '
                              'Default: autodetect'),
                        type=int, required=False)
    parser.add_argument('--ncpu',  dest='ncpu',
                        help='Number of cpu to use. Default: autodetect',
                        type=int, required=False)
    parser.add_argument('--no_archive',  dest='no_arch',
                        help='Set to avoid tar of output',
                        action='store_true')
    parser.add_argument('--tmp',  dest='tempdir',
                        help='Set the temporary folder',
                        default='/tmp')
    args = parser.parse_args()
    archive_res = not args.no_arch
    if args.mem:
        os.environ['PYPE_MEM'] = '%iG' % args.mem
    if args.ncpu:
        os.environ['PYPE_NCPU'] = '%i' % args.ncpu
    try:
        tempdir = os.environ['TEMPDIR']
    except KeyError:
        tempdir = args.tempdir

    log_dir = os.path.join(os.getcwd(), 'logs')
    log = ExtLog('run_csvc', log_dir, level=logging.INFO)

    log.log.info('Prepare temporary diirectory structure')
    prepare_tmp_dirs(tempdir, log, ['databases', 'data', 'workdir'])

    output_dir = '/tmp/workdir'
    results_dir = os.getcwd()

    log.log.info('Output results in folder %s' % output_dir)
    use_profile = 'default'
    log.log.info('Use profile %s' % use_profile)
    profile = get_profiles({})[use_profile]

    if os.path.splitext(args.ref_gz)[1] == '.gz':
        ref_dict = {'genome_fa_gz': args.ref_gz}
        bgzip_ref = False
    else:
        ref_dict = {'genome_fa': args.ref_gz}
        bgzip_ref = True
    create_symlinks(ref_dict, profile, log)

    setup_ref_files(args.cen_tel, args.cen_tel2, None,
                    args.gc1kb, bgzip_ref, profile, log)

    bam_files = setup_bams_bedpe(
        args.tumor_bam, args.tumor_bai, args.svs,
        args.tumor_cov, args.normal_cov, '/tmp/data', profile, log)
    out_dirs = [os.path.join(output_dir, 'rgs'),
                os.path.join(output_dir, 'classification'),
                os.path.join(output_dir, 'rgs', 'covplot')]
    for out_dir in out_dirs:
        if not os.path.isdir(out_dir):
            os.makedirs(out_dir)
    abs_svs = re.sub(
        '.bedpe$', '', re.sub('.gz$', '', os.path.basename(args.svs)))
    pype_cmd = ['pype', '--profile', use_profile, 'pipelines',
                '--queue', 'parallel', '--log', log_dir,
                'sv_complex_fast',
                '--aliquot_id', args.sample,
                '--tumor_bam', bam_files['tumor'],
                '--sequenza_segments', args.segs,
                '--sequenza_solutions', args.sols,
                '--breakpoints', bam_files['bedpe'],
                '--abs_breakpoints', os.path.join(
                    output_dir, 'rgs', '%s_abs_breakpoins.bedpe' % abs_svs),
                '--out_dir', os.path.join(output_dir, 'rgs'),
                '--tumor_cov', bam_files['tumor_cov'],
                '--control_cov', bam_files['normal_cov'],
                '--plot_dir', os.path.join(
                    output_dir, 'rgs', 'covplot'),
                ]

    pype_cmd = shlex.split(' '.join(map(str, pype_cmd)))
    log.log.info('Prepare pype command line:')
    log.log.info(' '.join(map(str, pype_cmd)))
    pype_proc = subprocess.Popen(pype_cmd)
    pype_proc.communicate()[0]

    if archive_res:
        rgs_dir = os.path.join(output_dir, 'rgs')

        rgs_res_file = os.path.join(
            results_dir, '%s_rgs_cn.tar.gz' % args.sample)
        log.log.info(
            'Create archive for results files in %s' % rgs_res_file)
        rgs_tar = tarfile.open(rgs_res_file, 'w:gz')
        rgs_tar.add(
            rgs_dir, arcname='rgs')
        rgs_tar.close()

        log.log.info('Archive log directory %s' % log_dir)
        tar = tarfile.open(
            os.path.join(results_dir, '%s_logs.tar.gz' % args.sample), 'w:gz')
        tar.add(log_dir, arcname='logs')
        tar.close()
    else:
        log.log.info('Skip archive of results')
    log.log.info('Done')

if __name__ == '__main__':
    main()
