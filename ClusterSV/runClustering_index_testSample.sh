#!/bin/bash
#PBS -o /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/output.out
#PBS -e /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/error.err
#PBS -l mem=32gb
#PBS -l walltime=1:00:00
#PBS -N yilong_testSample
#PBS -t 1


# get samples names from list.txt 
while read SAMPLE; do SAMPLES+=($SAMPLE); done < /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/list.txt
declare -p SAMPLES

# get sample name for the given job array ID 
INDEX=$(( $PBS_ARRAYID -1 ))
SAMPLE=${SAMPLES[$INDEX]}

# make uncompressed sample name
suffix=.gz
SAMPLE_UNCOMP=${SAMPLE%$suffix}

# create folder for the given job array ID
mkdir -p /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/${SAMPLE_UNCOMP}

# cp file to results folder
cp /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/${SAMPLE} /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/${SAMPLE_UNCOMP}/${SAMPLE}

# gunzip file
gunzip /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/${SAMPLE_UNCOMP}/${SAMPLE}

# remove every row containing chromosome Y in either chrom1 or chrom2 column
awk '$1 != "Y"' /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/${SAMPLE_UNCOMP}/${SAMPLE_UNCOMP} > output && mv output /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/${SAMPLE_UNCOMP}/${SAMPLE_UNCOMP}
awk '$4 != "Y"' /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/${SAMPLE_UNCOMP}/${SAMPLE_UNCOMP} > output && mv output /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/${SAMPLE_UNCOMP}/${SAMPLE_UNCOMP}

# remove header
sed -i '1d' /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/${SAMPLE_UNCOMP}/${SAMPLE_UNCOMP}

# find exact breakpoints
perl /home/projects/cu_10027/people/anviol/sv/yilong/BRASS/perl/bin/get_abs_bkpts_from_clipped_reads.pl \
--out /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/${SAMPLE_UNCOMP}/03b5268e-881e-49e7-824f-170c3fc8b11b.pcawg_consensus_1.6.161116.somatic.abs_bkpts.sv.bedpe \ # insert generic sample + abs_bkpts
--fasta /home/projects/cu_10027/data/genomes/UCSC/hg19/genome.fa \
/home/projects/cu_10027/projects/pcawg/data/data_processed/bam/03b5268e-881e-49e7-824f-170c3fc8b11b/tumor_ICGC_MB6_merged.mdup.bam \ # insert generic .bam
/home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/03b5268e-881e-49e7-824f-170c3fc8b11b.pcawg_consensus_1.6.161116.somatic.sv.bedpe/03b5268e-881e-49e7-824f-170c3fc8b11b.pcawg_consensus_1.6.161116.somatic.sv.bedpe # insert generic sample

# run script
#Rscript /home/projects/cu_10027/people/anviol/sv/yilong/ClusterSV/clustering_index.R /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/${SAMPLE_UNCOMP}/${SAMPLE_UNCOMP} 8
Rscript /home/projects/cu_10027/people/anviol/sv/yilong/ClusterSV/clustering_index.R /home/projects/cu_10027/people/anviol/sv/yilong/data/testSample/results/${SAMPLE_UNCOMP}/03b5268e-881e-49e7-824f-170c3fc8b11b.pcawg_consensus_1.6.161116.somatic.abs_bkpts.sv.bedpe 8 # insert generic sample
