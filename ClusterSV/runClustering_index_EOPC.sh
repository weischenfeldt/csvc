#!/bin/bash
#PBS -o /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/results/output.out
#PBS -e /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/results/error.err
#PBS -l mem=32gb
#PBS -l walltime=10:00:00
#PBS -N yilongSV
#PBS -t 1-318


# get samples names from list.txt 
while read SAMPLE; do SAMPLES+=($SAMPLE); done < /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/list.txt
declare -p SAMPLES

# get sample name for the given job array ID 
INDEX=$(( $PBS_ARRAYID -1 ))
SAMPLE=${SAMPLES[$INDEX]}

# make uncompressed sample name
suffix=.gz
SAMPLE_UNCOMP=${SAMPLE%$suffix}

# create folder for the given job array ID
mkdir -p /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/results/${SAMPLE_UNCOMP}

# cp file to results folder
cp /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/${SAMPLE} /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/results/${SAMPLE_UNCOMP}/${SAMPLE}

# gunzip file
gunzip /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/results/${SAMPLE_UNCOMP}/${SAMPLE}

# remove every row containing chromosome Y in either chrom1 or chrom2 column
awk '$1 != "Y"' /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/results/${SAMPLE_UNCOMP}/${SAMPLE_UNCOMP} > output && mv output /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/results/${SAMPLE_UNCOMP}/${SAMPLE_UNCOMP}
awk '$4 != "Y"' /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/results/${SAMPLE_UNCOMP}/${SAMPLE_UNCOMP} > output && mv output /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/results/${SAMPLE_UNCOMP}/${SAMPLE_UNCOMP}

# remove header
sed -i '1d' /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/results/${SAMPLE_UNCOMP}/${SAMPLE_UNCOMP}

# run script
Rscript /home/projects/cu_10027/people/anviol/sv/yilong/ClusterSV/clustering_index.R /home/projects/cu_10027/people/anviol/sv/yilong/data/EOPC/results/${SAMPLE_UNCOMP}/${SAMPLE_UNCOMP} 8
