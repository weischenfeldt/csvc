# CSVC Docker


In computerome use [udocker](https://github.com/indigo-dc/udocker) instad of docker


image available from docker hub:

```
udocker pull weischenfeldt/csvc:latest
```


## Share files udocker <--> computerome

To acess files (segmentstion, BAM, bedpe files...) you need to specify one or more directory
in the (u)docker command line.

As example you want to acees all the files in `~/data_test`:

```
udocker run -v /data:~/data_test csvc:latest /bin/bash
```

With this command all the files in `~/data_test` in your computerome home folder
will be available in the directory `/data` in the udocker container



