#!/usr/bin/env cwl-runner
class: CommandLineTool
id: csvc_workflow
label: csvc_workflow
cwlVersion: v1.0
dct:creator:
  '@id': http://orcid.org/0000-0003-3684-2659
  foaf:name: Francesco Favero
  foaf:mbox: francesco.favero@bric.ku.dk
requirements:
  - class: DockerRequirement
    dockerPull: registry.hub.docker.com/weischenfeldt/csvc:ppcg
hints:
  - class: ResourceRequirement
    coresMin: 4
    ramMin: 16384
    outdirMin: 512000
inputs:
  sample-id:
    type: string
    inputBinding:
      position: 1
      prefix: --sample-id
  tar-file:
    type: File
    inputBinding:
      position: 2
      prefix: --rg-tar
  solutions:
    type: File
    inputBinding:
      position: 3
      prefix: --solutions
  mem:
    type: ["null", int]
    inputBinding:
      position: 4
      prefix: --mem
  ncpu:
    type: ["null", int]
    inputBinding:
      position: 5
      prefix: --ncpu
outputs:
  archives:
    type:
      type: array
      items: File
    outputBinding:
      glob: "*.tar.gz"
baseCommand: [/usr/local/bin/run_classification.py]
doc: |
    ![build_status](https://img.shields.io/docker/build/weischenfeldt/csvc.svg)
    ![docker_pulls](https://img.shields.io/docker/pulls/weischenfeldt/csvc.svg)
    ![docker_builds](https://img.shields.io/docker/automated/weischenfeldt/csvc.svg)
 
    # cSVc workflow

    Docker container and pipeline for the Complex SV classification workflow


    ## Input Files

    In order for cSVc to run, the user needs to have the following input files:

      - PPCG Sample ID (Used to lookup information in the PPCG released metadata)
      - Tumor bam & index file
      - Breakpoints bedpe file (PPCG SV release)
      - Segment file (SCNA PPCG release, Battenberg)
      - Tumor & normal coverage file (**1Kb** cov from the EO-SV pipeline)
      - Centromere & telomere coordinate file (2 files, legacy dependency of various formats)
      - Genome reference fasta file
      - Cellularity & ploidy file (Metadata PPCG data release)

      

    The 2 centromeres/telomeres files used can be downloaded here:

      - https://bitbucket.org/weischenfeldt/csvc/raw/ppcg/ref_files/hg19_centromere_telomeres.tsv
      - https://bitbucket.org/weischenfeldt/csvc/raw/ppcg/ref_files/hg19_centromere_telomeres2.tsv

    The CWL input file will have the following information (eg: in yaml format, but JSON format is accepted as well):


    ```
    sample-id: SAMPLE_NAME
    ncpu: 4
    mem: 8
    tumor-bam:
      path: /path/to/tumor.bam
      class: File
    tumor-bam-index:
      path: /path/to/tumor.bam.bai
      class: File
    breakpoins:
      path: /path/to/breakpoints.bedpe
      class: File
    segments:
      path: /path/to/segments.txt
      class: File
    solutions:
      path: /path/to/Cellularity_Ploidy_Estimates_01_June_2020.csv.gz
      class: File
    tumor-cov:
      path: /path/to/tumor_coverage.cov.gz
      class: File
    normal-cov:
      path: path/to/normal_coverage.cov.gz
      class: File
    reference:
      path: /path/to/genome.fa.gz
      class: File
    centromere_telomeres:
      path: /path/to/centromere_telomeres.tsv
      class: File
    centromere_telomeres2:
      path: /path/to/centromere_telomeres2.tsv
      class: File
    ```

    See the `test.cwl.yaml` CWL input file in the code repository for a working example



    ## Running the workflow

    The workflow can be run using cwltool or Dockstore.
    It was tested using cwltool with Docker, uDocker and singularity:

    ### Docker

    If Docker is available it's the best option in term of resource utilization and speed


    ```
    cwltool csvc.cwl test.cwl.yaml
    ```

    ### uDocker

    A popular replacement for Docker is uDocker (https://github.com/indigo-dc/udocker).

    After installation (no root privileges required) if the udocker command is in the
    environment `PATH`, the workflow can be launched with the command

    ```
    cwltool --user-space-docker-cmd=udocker csvc.cwl test.cwl.yaml
    ```

    uDocker have a very large start time, as it need extensive operation to produce the  running container.


    ### Singularity

    A more efficient way is to run cwltool via `singularity`.

    In order to work properly it may need some optional environment variable to be set.
    In our setting singularity (version 3.5.3) was working with the following syntax:


    ```
    wd=$(PWD) # set this to a writable/temp folder

    mkdir -p $wd/tmp_database $wd/tmp_data
    export SINGULARITY_BINDPATH="$wd/tmp_data:/data,$wd/tmp_database:/databases"
    cwltool --singularity csvc.cwl test.cwl.yaml

    rm -rf $wd/tmp_data*
    ```

    Depending on the singularity installation it may be needed to manually cleanup
    temporary folder/container for each run


    ## Distribution post-run

    This pipeline will compute the required input for the complex SV classification.
    The classification step will need to be run in-house due to possible complications.

    After completion of the cSVc workflow, the relevant output will be stored in the
    `<SAMPLE_NAME>_rgs_cn.tar.gz file`. In order to run the classification algorithm,
    this compressed file should be sent to the Weischenfeldt Group or should be upload
    to the appropriate country folder at the EGA private ftp server.
